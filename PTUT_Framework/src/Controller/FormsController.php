<?php

declare(strict_types=1);


namespace App\Controller;

use Cake\ORM\Query;
use Cake\Database\Expression\QueryExpression;

/**
 * Forms Controller
 *
 * @method \App\Model\Entity\Form[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FormsController extends AppController
{

    public function getQuestionCount($arrayQuestion){

        $count = 0;
        foreach ($arrayQuestion as $value) {
            $count += $value['nb_questions'];
        }

        return $count;
    }

    public function getForms()
    {

        // Commence une nouvelle requête.
        
        $query = $this->Forms->find();
        $query->where(function (QueryExpression $exp, Query $query){
                    return $exp->or(['expire >=' => date("Y-m-d"),'expire IS NULL']);
                })
                ->group('Forms.id')
                ->order(['Forms.expire NOT LIKE \'\'' => 'DESC','Forms.expire'])
            ->contain(['Pages']);

        $search = $this->getRequest()->getQuery('search');
        if (!empty($search)) {

            $query->where(function (QueryExpression $exp, Query $query){
                return $exp->and([
                    "LOWER(Form.title) LIKE '%" . strtolower($this->getRequest->getQuery('search')) . "%'",
                    $query->newExpr()->or(['expire >=' => date("Y-m-d"),'expire IS NULL'])
                ]);
            });
        } else {

            $query->where(function (QueryExpression $exp, Query $query){
                return $exp->or(['expire >=' => date("Y-m-d"),'expire IS NULL']);
            }) ;
        }
        $query->group('Forms.id')
                ->order(['Forms.expire NOT LIKE \'\'' => 'DESC','Forms.expire'])
                ->contain(['Pages']);
        return $query;
    }

    /**
     * Home method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function home()
    {
        $session = $this->getRequest()->getSession();
        if (!$session->check('user')) {
            $session->destroy();
            $this->redirect('/');
        }

        $css = ['root', 'font', 'header', 'home-all_forms'];
        $this->set(compact('css'));

        $script = ['class_notification', 'home-all_forms'];
        $this->set(compact('script'));

        $pageTitle = 'Fill N Form | home';
        $this->set(compact('pageTitle'));

        //Get forms
        $forms = '';
        
        foreach ($this->getForms() as $value) {
            $countQ = $this->getQuestionCount($value->pages);
            $forms .=   '<div class="blocForm">
                            <div>
                                <a href="/form/' . $value['id'] . '">
                                    <p>Titre : ' . $value['title'] . '</p>
                                    
                                    <div>
                                        <img src="img/formulaire.png" alt="image form">
                                    </div>
                                    <p class="formID">ID #' . $value['id'] . '</p>' .
                (!empty($value['expire']) ? '<p>expire le ' . $value['expire'] ?? NULL . '</p>' : NULL)
                . '<p>' . $value['nb_page'] . ' page' . (($value['nb_page'] > 1) ? "s" : null) . '</p>
                                    <p>' . $countQ . ' question' . (($countQ > 1) ? "s" : null) . '</p>
                                </a>
                            </div>
                        </div>';
        }
        
        $this->set(compact('forms'));
    }

    /**
     * View method
     *
     * @param string|null $id Form id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $form = $this->Forms->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('form'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $form = $this->Forms->newEmptyEntity();
        if ($this->request->is('post')) {
            $form = $this->Forms->patchEntity($form, $this->request->getData());
            if ($this->Forms->save($form)) {
                $this->Flash->success(__('The form has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The form could not be saved. Please, try again.'));
        }
        $this->set(compact('form'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Form id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $form = $this->Forms->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $form = $this->Forms->patchEntity($form, $this->request->getData());
            if ($this->Forms->save($form)) {
                $this->Flash->success(__('The form has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The form could not be saved. Please, try again.'));
        }
        $this->set(compact('form'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Form id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $form = $this->Forms->get($id);
        if ($this->Forms->delete($form)) {
            $this->Flash->success(__('The form has been deleted.'));
        } else {
            $this->Flash->error(__('The form could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
