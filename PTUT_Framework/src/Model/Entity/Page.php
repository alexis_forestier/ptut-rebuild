<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity
 *
 * @property int $id
 * @property int $form_id
 * @property int $owner_id
 * @property int $nb_questions
 * @property string|null $title
 * @property string $color_form
 * @property string $color_page
 * @property string $color_font
 * @property string $color_input
 *
 * @property \App\Model\Entity\Form $form
 * @property \App\Model\Entity\User $user
 */
class Page extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'form_id' => true,
        'owner_id' => true,
        'nb_questions' => true,
        'title' => true,
        'color_form' => true,
        'color_page' => true,
        'color_font' => true,
        'color_input' => true,
        'form' => true,
        'user' => true,
    ];
}
