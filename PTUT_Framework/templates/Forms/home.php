<div>
    <?php
    //Create "Search" form 
    echo $this->Form->create(null, [
        'type' => 'get',
        'url' => [
            'action' => 'home'
        ],
        'class' => 'search'
    ]);

    echo '<input type="search" name="search" value="">';
    echo '<button type="submit"><span class="gg-search"></span></button>';

    echo $this->Form->end();
    ?>
</div>

<div>
    <?= $forms ?>
</div>