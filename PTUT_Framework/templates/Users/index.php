
<?php 
//Create "SIGN UP" form 
echo $this->Form->create(null, [
                                'type' => 'post',
                                'id' => 'form-signin', 
                                'url' => [
                                    'action' => 'add'
                                    ],
                                'class' => 'ResponsiveForm'
]);

echo "<h2>S'inscrire</h2>";

//First name
echo $this->Form->control('fname',[
    'type' => 'text', 
    'label' => __('First name'), 
    'id' => 'su_fname',
    'autocomplete' => 'off',
    'class' => 'ResponsiveInput'
]);

//Last name
echo $this->Form->control('lname',[
    'type' => 'text', 
    'label' => __('Last name'), 
    'id' => 'su_lname',
    'autocomplete' => 'off',
    'class' => 'ResponsiveInput'
]);

//Email
echo $this->Form->control('email',[
    'type' => 'email', 
    'label' => __('Email'), 
    'id' => 'su_email',
    'autocomplete' => 'off',
    'class' => 'ResponsiveInput'
]);

//Password
echo $this->Form->control('password',[
    'type' => 'password', 
    'label' => __('Password'), 
    'id' => 'su_password',
    'autocomplete' => 'off',
    'class' => 'ResponsiveInput'
]);

//btn-submit
echo $this->Form->button(__('Sign up'),[
    'type' => 'submit',
    'id' => 'b-sign-in',
    'class' => 'buttonAccueil ResponsiveButton'
]);


//End "SIGN UP" form
echo $this->Form->end();

?>

        <div id="blocLabel">
            <div id="slidL" class="slider">&laquo;</div>

            <div id="brand" class="">
                <img src="/img/logoIndex.png" alt="logo fill n form">
                <div id="menu">
                    <hr>
                    <div id=SignIn>
                        S'inscrire
                    </div>
                    <div id=SignInForm></div>
                    <div id=LogIn>
                        Se connecter
                    </div>
                    <div id=LogInForm></div>
                </div>
            </div>

            <div id="slidR" class="slider">&raquo;</div>
        </div>

<?php 

//Create "LOG IN" form 
echo $this->Form->create(null, [
                                'type' => 'post',
                                'id' => 'form-login', 
                                'url' => [
                                    'action' => 'connect'
                                    ],
                                'class' => 'ResponsiveForm'
]);

echo "<h2>Se connecter</h2>";


//Email
echo $this->Form->control('email',[
    'type' => 'email', 
    'label' => __('Email'), 
    'id' => 'li_email',
    'autocomplete' => 'off',
    'class' => 'ResponsiveInput'
]);

//Password
echo $this->Form->control('password',[
    'type' => 'password', 
    'label' => __('Password'), 
    'id' => 'li_password',
    'autocomplete' => 'off',
    'class' => 'ResponsiveInput'
]);

//TODO add checkbox remenber-me

//btn-submit
echo $this->Form->button(__('Log in'),[
    'type' => 'submit',
    'id' => 'b-login',
    'class' => 'buttonAccueil ResponsiveButton'
]);


//End "LOG IN" form
echo $this->Form->end();
