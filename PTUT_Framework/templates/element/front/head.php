<head>
    <title><?= $pageTitle ?></title>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
        if(!empty($css)){
            echo $this->Html->css($css);
        } 
    ?>
    <?= $this->Html->meta('icon') ?>

</head>