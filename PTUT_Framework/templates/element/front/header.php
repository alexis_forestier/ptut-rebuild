
<?php  

    if($this->request->getParam('_matchedRoute') != '/'):
?>
<header>

    <nav>
        <ul>
            
            <li><div><?= $this->Html->image('favicon.png', ['alt' => 'Fill-N-Form logo', 'id' => 'headerlogo']) ?></div><span></span></li>
            <li><?= $this->Html->link('Tous les Forms','/home') ?><span></span></li>
            <li><a href="/CreateForm.php">Créer un nouveau formulaire</a><span></span></li>
            <li><a href="/dashboard.php">Tableau de bord</a><span></span></li>
            <li><?= $this->Html->link('Se déconnecter','/logout') ?><span></span></li>

        </ul>
    </nav>

</header>
<?php  
    else:
?>
<header></header>
<div id="BackgroundAnime"></div>
<?php  
    endif
?>