<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

    //CSS from controller
    $css = $css ?? 'style';

    //Script from controller
    $script = $script ?? '';

    //Title from controller
    $pageTitle = $pageTitle ?? __('no title')
?>
<!DOCTYPE html>
<html>

<!--HEAD-->
<?= $this->element('front/head', array(
    'pageTitle' => $pageTitle, 
    'css' => $css
)) ?>

<body>
    <!--HEADER-->
    <?= $this->element('front/header') ?>

    <main class="main">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </main>

    <!--FOOTER-->
    <?= $this->element('front/footer') ?>
    

    <!--SCRIPT-->
    <?php
        if(!empty($script)){
            echo $this->Html->script($script);
        } 
    ?>

</body>

</html>