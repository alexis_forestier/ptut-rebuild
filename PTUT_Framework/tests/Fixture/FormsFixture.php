<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FormsFixture
 */
class FormsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'owner_id' => 1,
                'nb_page' => 1,
                'title' => 'Lorem ipsum dolor sit amet',
                'expire' => '2022-02-10',
                'status' => 'Lorem ipsum dolor sit amet',
                'created' => 1644526045,
                'modified' => 1644526045,
            ],
        ];
        parent::init();
    }
}
