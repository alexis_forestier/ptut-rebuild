<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PagesFixture
 */
class PagesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'form_id' => 1,
                'owner_id' => 1,
                'nb_questions' => 1,
                'title' => 'Lorem ipsum dolor sit amet',
                'color_form' => 'Lorem ipsum do',
                'color_page' => 'Lorem ipsum do',
                'color_font' => 'Lorem ipsum do',
                'color_input' => 'Lorem ipsum do',
            ],
        ];
        parent::init();
    }
}
