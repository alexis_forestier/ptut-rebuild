function layoutVisuAllForms() {
    let n = Math.round(window.innerWidth * 2 / 500)
    document.documentElement.style.setProperty('--layoutVisuAllForms', n)
}

window.addEventListener('resize', layoutVisuAllForms)
layoutVisuAllForms()