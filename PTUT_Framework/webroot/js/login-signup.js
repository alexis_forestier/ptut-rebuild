
let inputs = document.querySelectorAll('main form > div input');
console.log(inputs)
function inputNotEmpty(input) {
    
    let label = input.parentNode.firstElementChild;
    if (input.value != "") {
        label.classList.add('notEmpty');
    } else {
        label.classList.remove('notEmpty');
    }
}

for (let index = 0; index < inputs.length; index++) {
    inputs[index].addEventListener('keyup', inputNotEmpty.bind(null,inputs[index]));
    inputNotEmpty(inputs[index])
}

let main = document.querySelector('#LogInSignUp main');
let menu = document.getElementById('menu');
let Msign = document.getElementById('form-signin');
let Mlog = document.getElementById('form-login');
let sign = document.getElementById('SignInForm');
let log = document.getElementById('LogInForm');

function windowResize() {
    if (window.innerWidth < 1330) {
        menu.style.display = 'flex';
        if (sign.innerHTML == "") {
            let s = Msign.cloneNode(true);
            sign.appendChild(s)
            sign.firstChild.style.display = 'none';
        }
        if (log.innerHTML == "") {
            let l = Mlog.cloneNode(true);
            log.appendChild(l)
            log.firstChild.style.display = 'none';
        }

    } else {
        menu.style.display = 'none';
        if (sign.innerHTML != "" && log.innerHTML != "") {
            sign.innerHTML = log.innerHTML = "";
        }
    }
}

windowResize();
window.addEventListener('resize', windowResize);

let SignisOpen = false;
let inputsSign = document.querySelectorAll('#SignInForm form > div input');

function openSign() {
    if (SignisOpen) {
        sign.firstChild.setAttribute('style', 'display : none;');
        SignisOpen = false;
        for (let index = 0; index < inputsSign.length; index++) {
            inputsSign[index].removeEventListener('keyup', inputNotEmpty.bind(null,inputsSign[index]));
        }
    } else {
        sign.firstChild.removeAttribute('style');
        SignisOpen = true;
        if (LogisOpen) {
            openLog()
        }
        for (let index = 0; index < inputsSign.length; index++) {
            inputsSign[index].addEventListener('keyup', inputNotEmpty.bind(null,inputsSign[index]));
        }
    }
}

let LogisOpen = false;
let inputsLog = document.querySelectorAll('#LogInForm form > div input');

function openLog() {
    if (LogisOpen) {
        log.firstChild.setAttribute('style', 'display : none;');
        LogisOpen = false;
        for (let index = 0; index < inputsLog.length; index++) {
            inputsLog[index].removeEventListener('keyup', inputNotEmpty.bind(null,inputsLog[index]));
        }
    } else {
        log.firstChild.removeAttribute('style');
        LogisOpen = true;
        if (SignisOpen) {
            openSign()
        }
        for (let index = 0; index < inputsLog.length; index++) {
            inputsLog[index].addEventListener('keyup', inputNotEmpty.bind(null,inputsLog[index]));
        }
    }
}

document.getElementById('SignIn').addEventListener('click', openSign);
document.getElementById('LogIn').addEventListener('click', openLog);